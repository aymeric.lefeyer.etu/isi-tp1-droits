# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- LE FEYER, Aymeric, email: aymeric.lefeyer.etu@univ-lille.fr

- LEDAIN, Alexy, email: alexy.ledain.etu@univ-lille.fr

## Question 1

`adduser toto`  
`groupadd ubuntu`  
`usermod -a -G ubuntu toto`  
`passwd toto` (totototo)  
`su - toto`

`-r--rw-r--`  
Il s'agit d'un fichier  
Il peut lire mais ne peux pas écrire ni éxecuter

L'utilisateur toto peux lire mais ne peux pas écrire  
Les permissions du propriétaire sont prioritaires sur les permissions du groupe

Réponse NON

## Question 2

La permission x sur un répertoire permet de rentrer dans ce répertoire

`mkdir mydir`  
`chgrp ubuntu mydir`  
`chmod g-x mydir`  
`cd mydir`

La permission est refusée, on ne peut pas accéder car on a pas les droits

`ls -al mydir`

```bash
ls: impossible d'accéder à 'mydir/.': Permission non accordée
ls: impossible d'accéder à 'mydir/..': Permission non accordée
total 0
d????????? ? ? ? ?              ? .
d????????? ? ? ? ?              ? ..
```

L'utilisateur n'a pas l'accès au dossier donc impossible d'en voir le contenu

## Question 3

Le code se trouve dans question3/myopen.c  
Un makefile permet de l'éxecuter

On arrive pas à ouvrir le fichier

Avec le flag, on arrive à ouvrir le fichier  
On a 1001 pour chaque ID

## Question 4

Le code se trouve dans questions4/suid.py  
Pour executer le code, il faut entrer la commande `python suid.py`

En python, on ne peux pas ouvrir le fichier car la commande python n'est pas autorisée à ouvrir ce fichier

## Question 5

La commande `ls -al /usr/bin/chfn` renvoie

```bash
-rws--x--x. 1 root root 32760  7 janv. 11:21 /usr/bin/chfn
```

L'utilisateur root peut écrire, lire et éxecuter  
Le groupe root peut uniquement éxecuter

Avant de modifier l'utilisateur toto, on avait cette ligne via le `cat /etc/passwd`  
`toto:x:1001:1001::/home/toto:/bin/bash`

En effectuant `chfn` sur toto, il nous propose de modifier le Bureau et les numéros de téléphones, enfin on trouve cette ligne sur le `cat /etc/passwd`
`toto:x:1001:1001:,Buro,03,06:/home/toto:/bin/bash`

## Question 6

Les mots de passes sont stockés dans `/etc/shadow` mais ils sont cryptés, pour des questions de sécurité évidentes

## Question 7

Créations des utilisateurs. (mdp = 123456789)
On créé lambda_a_bis et lambda_b_bis, appartenant respectivements aux groupes groupe_a et groupe_b.  
Il est essentiel de créer ces utilisateurs pour tester la modification dans le même groupe avec un utilisateur différent

```
[root@localhost aymeric]# adduser lambda_a
[root@localhost aymeric]# adduser lambda_b
[root@localhost aymeric]# adduser admin
[root@localhost aymeric]# adduser lambda_admin
[root@localhost aymeric]# adduser lambda_a_bis
[root@localhost aymeric]# adduser lambda_b_bis
[root@localhost aymeric]# groupadd groupe_a
[root@localhost aymeric]# groupadd groupe_b
[root@localhost aymeric]# usermod -a -G groupe_a lambda_a
[root@localhost aymeric]# usermod -a -G groupe_a lambda_a_bis
[root@localhost aymeric]# usermod -a -G groupe_b lambda_b
[root@localhost aymeric]# usermod -a -G groupe_b lambda_b_bis
[root@localhost aymeric]$ usermod -a -G groupe_a lambda_admin
[root@localhost aymeric]$ usermod -a -G groupe_b lambda_admin
[root@localhost aymeric]# passwd lambda_a
[root@localhost aymeric]# passwd lambda_b
[root@localhost aymeric]# passwd lambda_a_bis
[root@localhost aymeric]# passwd lambda_b_bis
[root@localhost aymeric]# passwd lambda_admin
```

Créations des dossiers

```
[aymeric@localhost home]$ sudo mkdir dir_a
[aymeric@localhost home]$ sudo mkdir dir_b
[aymeric@localhost home]$ sudo mkdir dir_c
[aymeric@localhost home]$ sudo chgrp groupe_a dir_a
[aymeric@localhost home]$ sudo chgrp groupe_b dir_b
[aymeric@localhost home]$ sudo chown lambda_admin dir_c
[aymeric@localhost home]$ sudo chmod u+rwx dir_a
[aymeric@localhost home]$ sudo chmod u+rwx dir_b
[aymeric@localhost home]$ sudo chmod u+rwx dir_c
[aymeric@localhost home]$ sudo chmod o+r dir_c
[aymeric@localhost home]$ sudo chmod o-rwx dir_a
[aymeric@localhost home]$ sudo chmod o-rwx dir_b
[aymeric@localhost home]$ usermod -a -G groupe_a lambda_admin
[aymeric@localhost home]$ sudo chmod u+rwx dir_a
[aymeric@localhost home]$ sudo chmod u+rwx dir_b
[aymeric@localhost home]$ sudo chmod g+rwx dir_a
[aymeric@localhost home]$ sudo chmod g+rwx dir_b
```

Résultat

```
801936  0 drwxrwx---. 1 root         groupe_a        32 20 janv. 15:05 dir_a
801937  0 drwxrwx---. 1 root         groupe_b        40 20 janv. 15:07 dir_b
801938  0 drwxr-xr-x. 1 lambda_admin root             0 20 janv. 14:53 dir_c

```

Mettre les scripts bash dans le repertoire _question7_.

## Question 8

Le programme et les scripts dans le repertoire _question8_.

## Question 9

Le programme et les scripts dans le repertoire _question9_.

## Question 10

Les programmes _groupe_server_ et _groupe_client_ dans le repertoire
_question10_ ainsi que les tests.
