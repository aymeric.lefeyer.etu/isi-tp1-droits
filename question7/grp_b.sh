#!/bin/bash

cd ..

if cd dir_b 2>/dev/null
then
  echo "Access granted to dir_b"
else
  echo "Access denied to dir_b"
fi

if touch test 2>/dev/null
then
  echo "File created at dir_b"
else
  echo "File creation failed at dir_b"
fi

cd ..

if cd dir_c 2>/dev/null
then
  echo "Access granted to dir_c"
else
  echo "Access denied to dir_c"
fi

if touch test 2>/dev/null
then
  echo "File created at dir_c"
else
  echo "File creation failed at dir_c"
fi

echo "Setup ok"

