#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    FILE *f;
    int caractereActuel = 0;
    if (argc < 2)
    {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }
    f = fopen(argv[1], "r");
    if (f == NULL)
    {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File opens correctly\n");
    do
    {
        caractereActuel = fgetc(f);
        printf("%c", caractereActuel);
    } while (caractereActuel != EOF);
    printf("EUID is %d\n", geteuid());
    printf("EGID is %d\n", getegid());
    printf("RUID is %d\n", getuid());
    printf("RGID is %d\n", getgid());

    fclose(f);
    exit(EXIT_SUCCESS);
}