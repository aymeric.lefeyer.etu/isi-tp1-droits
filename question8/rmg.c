#include <stdio.h>
#include <stdlib.h>
#include <grp.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int readFile(char *path);
void printInfos(const char *path);
int getGID(char *path);

int main(int argc, char *argv[])
{
    // Infos du user
    char pwd[20] = {0};
    char *username = getenv("USER");

    // ID du groupe
    gid_t gid = getgid();

    // Recup paramètre
    char *filename = argv[1];
    printf("Fichier à supprimer: %s\n", filename);

    // Verifier que le fichier existe
    if (access(filename, F_OK) == -1)
    {
        printf("Ce fichier n'existe pas");
        return -1;
    }

    // printf("Group ID of current user : %d\n", gid);

    // Verifier si l'user appartient au groupe
    // printInfos(filename);

    // C'est les deux mêmes ?
    if (gid != getGID(filename))
    {
        printf("Vous n'avez pas les permissions pour supprimer ce fichier\n");
        return -1;
    }

    // Si c'est bon, on demande le pwd
    printf("Mot de passe ? \n");
    scanf("%s", pwd);

    // On verifie le pwd
    printf("Mot de passe saisi : %s\n", pwd);
    printf("Vérification de l'association avec l'user %s en cours ...\n", username);

    readFile(filename);

    // On supprime le fichier

    return 0;
}

void printInfos(const char *path)
{
    struct stat ret;

    if (stat(path, &ret) < 0)
        return;

    printf("Information for %s\n", path);
    printf("---------------------------\n");
    printf("File Size: \t\t%d bytes\n", ret.st_size);
    printf("Number of Links: \t%d\n", ret.st_nlink);
    printf("File inode: \t\t%d\n", ret.st_ino);

    printf("File Permissions: \t");
    printf((S_ISDIR(ret.st_mode)) ? "d" : "-");
    printf((ret.st_mode & S_IRUSR) ? "r" : "-");
    printf((ret.st_mode & S_IWUSR) ? "w" : "-");
    printf((ret.st_mode & S_IXUSR) ? "x" : "-");
    printf((ret.st_mode & S_IRGRP) ? "r" : "-");
    printf((ret.st_mode & S_IWGRP) ? "w" : "-");
    printf((ret.st_mode & S_IXGRP) ? "x" : "-");
    printf((ret.st_mode & S_IROTH) ? "r" : "-");
    printf((ret.st_mode & S_IWOTH) ? "w" : "-");
    printf((ret.st_mode & S_IXOTH) ? "x" : "-");
    printf("\n\n");
    struct group *gr = getgrgid(ret.st_gid);
    printf("Group Name : %d", gr->gr_gid);
    printf("\n\n");
}

int getGID(char *path)
{
    struct stat ret;

    if (stat(path, &ret) < 0)
        return 1;

    struct group *gr = getgrgid(ret.st_gid);
    return (gr->gr_gid);
}

//remove(“file_name”);

int readFile(char *path)
{
    FILE *fichier = NULL;
    int caractereActuel = 0;

    fichier = fopen("/home/lambda_admin/passwd", "r");

    if (fichier != NULL)
    {
        // Boucle de lecture des caractères un à un
        do
        {
            caractereActuel = fgetc(fichier); // On lit le caractère
            printf("%c", caractereActuel);    // On l'affiche
        } while (caractereActuel != EOF);     // On continue tant que fgetc n'a pas retourné EOF (fin de fichier)

        fclose(fichier);
    }

    return 0;
}

//you can change this to be the uid that you want

/**struct passwd *pw = getpwuid(uid);
if (pw == NULL)
{
    perror("getpwuid error: ");
}

int ngroups = 0;

//this call is just to get the correct ngroups
getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);
__gid_t groups[ngroups];

//here we actually get the groups
getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);

//example to print the groups name
for (int i = 0; i < ngroups; i++)
{
    struct group *gr = getgrgid(groups[i]);
    if (gr == NULL)
    {
        perror("getgrgid error: ");
    }
    printf("%s\n", gr->gr_name);
}
** /






**/